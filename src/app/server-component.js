import { getData } from "./lib/data.js";

export default async function ServerComponent() {
  const data = await getData();

  return (
    <div>
      <h1>Server Component</h1>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </div>
  );
}
