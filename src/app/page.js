import ClientComponent from "./client-component";
import ServerComponent from "./server-component";

export default function Home() {
  return (
    <div>
      <h1>Home</h1>
      <ClientComponent>
        <ServerComponent />
        <p>Client Component</p>
      </ClientComponent>
    </div>
  );
}
